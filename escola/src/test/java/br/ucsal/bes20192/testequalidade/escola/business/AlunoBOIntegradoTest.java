package br.ucsal.bes20192.testequalidade.escola.business;

import builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AlunoBOIntegradoTest {

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 tera 16
	 * anos. Caso de teste
	 * # | entrada 				 | saida esperada
	 * 1 | aluno nascido em 2003 |	 * 16
	 */
	@Test
	public void testarCalculoIdadeAluno1() {
		AlunoBuilder alunoBuilder = AlunoBuilder.umAluno();
		Aluno aluno = alunoBuilder.build();

		AlunoBO alunoBO;
		alunoBO = new AlunoBO(alunoBuilder.umAlunoDAO(), new DateHelper());

		int idadeAtual = alunoBO.calcularIdade(aluno.getMatricula());

		Assertions.assertEquals(16, idadeAtual);

	}

	/**
	 * Verificar se alunos ativos sao atualizados.
	 */
	@Test
	public void testarAtualizacaoAlunosAtivos() {

	}

}
