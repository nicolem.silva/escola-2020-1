package builder;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;

public class AlunoBuilder {

    private static final Integer MATRICULA_DEFAULT = 1;
    private static final  String NOME_DEFAULT = "José";
    private static final SituacaoAluno SITUACAO_DEFAULT = SituacaoAluno.ATIVO;
    private static final Integer ANONASCIMENTO_DEFAULT = 2003;

    private Integer matricula =  MATRICULA_DEFAULT;
    private String nome = NOME_DEFAULT;
    private SituacaoAluno situacao = SITUACAO_DEFAULT;
    private Integer anoNascimento = ANONASCIMENTO_DEFAULT;

    private AlunoBuilder(){

    }

    public static AlunoBuilder umAluno(){
        return new AlunoBuilder();
    }

    public AlunoBuilder comMatricula(int matricula){
        this.matricula = matricula;
        return this;
    }

    public AlunoBuilder comNome(String nome){
        this.nome = nome;
        return this;
    }

    public AlunoBuilder comSituacao(SituacaoAluno situacao){
        this.situacao = situacao;
        return this;
    }

    public AlunoBuilder comAnoNascimento(int ano){
        this.anoNascimento = ano;
        return this;
    }

    public AlunoBuilder mas(){
        return umAluno().comMatricula(matricula).comNome(nome)
                .comSituacao(situacao).comAnoNascimento(anoNascimento);
    }

    public Aluno build(){
        Aluno aluno = new Aluno();
        aluno.setMatricula(matricula);
        aluno.setNome(nome);
        aluno.setSituacao(situacao);
        aluno.setAnoNascimento(anoNascimento);

        AlunoDAO dao = new AlunoDAO();
        dao.salvar(aluno);
        return aluno;
    }

    public AlunoDAO umAlunoDAO(){
        AlunoBuilder alunoBuilder = new AlunoBuilder();

        AlunoDAO dao = new AlunoDAO();
        dao.salvar(alunoBuilder.build());
        return dao;
    }


}
